#pragma once
#include <windows.h>

#include "ShaderProgram.h"
#include "Mesh.h"
#include "Texture.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "Timer.h"

#define WINDOW_WIDTH			1024
#define WINDOW_HEIGHT			768
#define FRAMES_PER_SECOND		60

enum OBJECT_DISPLAY_MODE
{
	OBJECT_DISPLAY_NORMAL,
	OBJECT_DISPLAY_MODIFIED,
	OBJECT_DISPLAY_BOTH
};

enum OBJECT_MODIFY_MODE
{
	OBJECT_MODIFY_DUPLICATE,
	OBJECT_MODIFY_INVERT,
	OBJECT_MODIFY_BOTH
};

enum OBJECT_ADD_FEATURE
{
	OBJECT_ADD_NOTHING,
	OBJECT_ADD_NORMALS,
	OBJECT_ADD_FACE_NORMALS,
	OBJECT_ADD_WIREFRAME
};

class Game
{
public:
	Game();
	~Game();

	void initializeGame();
	void update();
	void draw();

	void TriangleTest(bool isInit);
	void SetObjectModifications(OBJECT_MODIFY_MODE);
	void AddObjectFeatures(OBJECT_ADD_FEATURE);

	void keyboardDown(unsigned char key, int mouseX, int mouseY);
	void keyboardUp(unsigned char key, int mouseX, int mouseY);
	void mouseClicked(int button, int state, int x, int y);
	void mouseMoved(int x, int y);

	Timer *timer = nullptr;
	float totalGameTime = 0.0f;

	Mesh* objectActive;

	Texture tex;

	ShaderProgram progSimpleDisplay;
	ShaderProgram progGeometryAlter;
	ShaderProgram progGeometryFeatures;
	ShaderProgram* progActiveSP;

	mat4 cameraTransform;
	mat4 cameraProjection;
	mat4 objectTransform;
	
private:

	GLuint vao;						// VAO for primitives
	GLuint vbo;						// VBO for primitives
	bool isTriangleUsed = false;	// flag to signal if triangle is used for demo
	
	float yGeomMod = 0;				// geometry modifier for Y coordinate

	// mode used to display object (mode is normal = original object, or altered = object with additional geometry)
	OBJECT_DISPLAY_MODE displayMode;
	
	bool isDuplicated = false;		// is object duplicated
	bool isInverted = false;		// is object duplicated and then copy inverted

	bool isShowWireFrame = false;	// show wireframe instead of full geometry?
	bool hasNormals = false;		// are normals displayed for the vertices
	bool hasFaceNormals = false;	// are normals displayed for the faces
	float normalLength = 0.25;		// length of line representing normal

	int objectActiveIndex = 0;	// index of currently active object

	float angle = 0;
};
