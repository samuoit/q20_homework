#include "Game.h"

int primitivesCount;
const char* primitiveFilePath[6];
Mesh objectPrimitive[6];

Game::Game()
{
	// display mode (normal or modified geometry)
	displayMode = OBJECT_DISPLAY_BOTH;
	
	objectActive = nullptr;	// pointer to active object
	objectActiveIndex = 0;	// index of active object

	primitivesCount = 6;	// count of objects (primitive objects)
	
	// file paths to primitive objects
	primitiveFilePath[0] = "./Assets/Models/Cone.obj";
	primitiveFilePath[1] = "./Assets/Models/Cube.obj";
	primitiveFilePath[2] = "./Assets/Models/Cylinder.obj";
	primitiveFilePath[3] = "./Assets/Models/Pyramid.obj";
	primitiveFilePath[4] = "./Assets/Models/Sphere.obj";
	primitiveFilePath[5] = "./Assets/Models/Torus.obj";

	// ***** offset on Y axis for modified geometry
	yGeomMod = 1.0f;
	// *****

}

Game::~Game()
{
	delete timer;
	progSimpleDisplay.UnLoad();
	progGeometryAlter.UnLoad();
	for (int i = 0; i < primitivesCount; i++) objectPrimitive[i].Unload();
	tex.Unload();
}

void Game::AddObjectFeatures(OBJECT_ADD_FEATURE feature)
{
	isShowWireFrame = false;
	hasFaceNormals = false;
	hasNormals = false;

	if (OBJECT_ADD_NORMALS == feature) hasNormals = true;
	else if (OBJECT_ADD_FACE_NORMALS == feature) hasFaceNormals = true;
	else if (OBJECT_ADD_WIREFRAME == feature) isShowWireFrame = true;
}

void Game::SetObjectModifications(OBJECT_MODIFY_MODE modifyMode)
{
	if (OBJECT_MODIFY_BOTH == modifyMode)
	{
		isDuplicated = true;
		isInverted = true;
	}
	else if (OBJECT_MODIFY_DUPLICATE == modifyMode)
	{
		isDuplicated = true;
		isInverted = false;
	}
	else if (OBJECT_MODIFY_INVERT == modifyMode)
	{
		isDuplicated = false;
		isInverted = true;
	}
}

//// FOR TRIANGLE PRIMITIVE DEMO ///
void Game::TriangleTest(bool isInit)
{
	// If init - then initialize object
	if (isInit || !(vao))
	{
		// triangle data
		GLfloat triangle[24] = { 0.0, 0.0, 0.5, 0.0, 0.5, 0.5, 0.0, 0.0, 0.0, 0.5, 0.5, 0.5, 0.0, 0.0, 0.5, 0.0, 0.5, -0.5, 0.0, 0.0, 0.0, -0.5, 0.5, -0.5 };

		// generate and bind VAO
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		// enable attribute data pointer
		glEnableVertexAttribArray(0);

		// generate and bind VBO
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, 24, triangle, GL_STATIC_DRAW);

		// set pointer to attribute data
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);

		// unbind VAO and VBO
		glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
		glBindVertexArray(GL_NONE);
	}
	else
	{
		mat4 tempTransform1, tempTransform2;

		// transform for non-modified triangle
		tempTransform1.RotateX(angle);
		tempTransform1.RotateY(angle);
		tempTransform1.Translate(vec3(2.0f, 0.0f, 0.0f));

		// transform for modified triangle
		tempTransform2.RotateX(angle);
		tempTransform2.RotateY(angle);
		tempTransform2.Translate(vec3(-2.0f, 0.0f, 0.0f));

		tex.Bind();

		// activate simple shader program, bind shader program, and send uniforms
		progActiveSP = &progSimpleDisplay;
		progActiveSP->Bind();
		progActiveSP->SendUniformMat4("uModel", tempTransform1.data, true);
		progActiveSP->SendUniformMat4("uView", cameraTransform.GetInverse().data, true);
		progActiveSP->SendUniformMat4("uProj", cameraProjection.data, true);
		progActiveSP->SendUniform("uTex", 0);

		// drawing of non-modified triangle
		glBindVertexArray(vao);
		glDrawArrays(GL_TRIANGLES, 0, 24);
		glBindVertexArray(0);

		progActiveSP->UnBind();

		// change active shader program, bind shader program, and send uniforms
		progActiveSP = &progGeometryAlter;
		progActiveSP->Bind();
		progActiveSP->SendUniformMat4("uModel", tempTransform2.data, true);
		progActiveSP->SendUniformMat4("uView", cameraTransform.GetInverse().data, true);
		progActiveSP->SendUniformMat4("uProj", cameraProjection.data, true);
		progActiveSP->SendUniform("uTex", 0);
		progActiveSP->SendUniform("uIsDuplicated", isDuplicated);
		progActiveSP->SendUniform("uIsInverted", isInverted);

		// change dynamically offset 
		yGeomMod = angle / 360;
		progActiveSP->SendUniform("uYmod", yGeomMod);

		// drawing of modified triangle
		glBindVertexArray(vao);
		glDrawArrays(GL_TRIANGLES, 0, 24);
		glBindVertexArray(0);

		progActiveSP->UnBind();

		// if there is a need for special feature then display it by using third shader program
		if (isShowWireFrame || hasNormals || hasFaceNormals)
		{
			progActiveSP = &progGeometryFeatures;
			progActiveSP->Bind();
			progActiveSP->SendUniformMat4("uModel", objectTransform.data, true);
			progActiveSP->SendUniformMat4("uView", cameraTransform.GetInverse().data, true);
			progActiveSP->SendUniformMat4("uProj", cameraProjection.data, true);
			progActiveSP->SendUniform("uTex", 0);

			progActiveSP->SendUniform("uIsShowWireFrame", isShowWireFrame);		// should wireframe be displayed?
			progActiveSP->SendUniform("uHasNormals", hasNormals);				// will normals will be displayed on each vertex?
			progActiveSP->SendUniform("uHasFaceNormals", hasFaceNormals);		// will normals be displayed on each face?
			progActiveSP->SendUniform("uNormalLength", normalLength);			// what is the size of line representing normal i.e. normal line length

			glBindVertexArray(objectActive->VAO);
			glDrawArrays(GL_TRIANGLES, 0, objectActive->GetNumVertices());
			glBindVertexArray(0);

			progActiveSP->UnBind();
		}
		tex.Unbind();
	}
}

void DisplayUserInfo()
{
	system("cls");	// clear screen
	std::cout << "" << std::endl;
	std::cout << "-------------------------------------------------------" << std::endl;
	std::cout << "User Controls:" << std::endl;
	std::cout << "-------------------------------------------------------" << std::endl;
	std::cout << "--- Object Selection ----------------------------------" << std::endl;
	std::cout << "  0: use triangle" << std::endl;
	std::cout << "1-6: switch between 3D basic objects" << std::endl;
	std::cout << "-------------------------------------------------------" << std::endl;
	std::cout << "--- Display Modes -------------------------------------" << std::endl;
	std::cout << "  B: use both object displays (normal and modified)" << std::endl;
	std::cout << "  N: use normal object display" << std::endl;
	std::cout << "  M: use modified object display" << std::endl;
	std::cout << "-------------------------------------------------------" << std::endl;
	std::cout << "--- Object Geometry Modifications ---------------------" << std::endl;
	std::cout << "  U: create object duplicate with offset and invert duplicate with offset" << std::endl;
	std::cout << "  I: create invert duplicate of object with offset" << std::endl;
	std::cout << "  O: create regular object duplicate with offset" << std::endl;
	std::cout << "-------------------------------------------------------" << std::endl;
	std::cout << "--- Additional Features -------------------------------" << std::endl;
	std::cout << "  W: Display wireframe" << std::endl;
	std::cout << "  C: Clear normals / wireframe" << std::endl;
	std::cout << "  V: Display vertex normals" << std::endl;
	std::cout << "  F: Display face normals" << std::endl;
	std::cout << "  .: Increase normals length" << std::endl;
	std::cout << "  ,: Decrease normals length" << std::endl;
	std::cout << "-------------------------------------------------------" << std::endl;
}

void Game::initializeGame()
{
	// initialize timer to track time elapsed
	timer = new Timer();

	// enable depth testing
	glEnable(GL_DEPTH_TEST);

	// load shader program for simple object display
	if (!progSimpleDisplay.Load("./Assets/Shaders/PassThrough.vert", "./Assets/Shaders/PassThrough.frag"))
	{
		std::cout << "Shader program failed to initialize.\n";
		system("pause");
		exit(0);
	}

	// load shader program for geometry alteration
	if (!progGeometryAlter.Load("./Assets/Shaders/GPassThrough.vert", "./Assets/Shaders/GModGeometry.geom", "./Assets/Shaders/GPassThrough.frag"))
	{
		std::cout << "Shader program failed to initialize.\n";
		system("pause");
		exit(0);
	}

	// load shader program for geometry visual enhancement via normals, face normals and wireframe
	if (!progGeometryFeatures.Load("./Assets/Shaders/GPassThrough.vert", "./Assets/Shaders/GNormalsGeometry.geom", "./Assets/Shaders/GPassThrough.frag"))
	{
		std::cout << "Shader program failed to initialize.\n";
		system("pause");
		exit(0);
	}

	// load all objects (primitives)
	for (int i = 0; i < primitivesCount; i++)
	{
		if (!objectPrimitive[i].LoadFromFile(primitiveFilePath[i]))
		{
			std::cout << "Model failed to load.\n";
			system("pause");
			exit(0);
		}
	}

	// load textue
	if (!tex.Load("./Assets/Textures/uvmap.png")) std::cout << "Failed to load the texture.\n";

	// set active object
	objectActive = &objectPrimitive[objectActiveIndex];

	// set camera's position, rotation and projection
	cameraTransform.Translate(0.0f, 0.0f, 5.0f);
	cameraProjection.FrustumProjection(60.0f, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 1.0f, 10000.0f);

	// initialize triangle data
	TriangleTest(true);

	// show info about user controls
	DisplayUserInfo();
}

void Game::update()
{
	// update our clock so we have the delta time since the last update
	timer->tick();

	float deltaTime = timer->getElapsedTimeSeconds();
	totalGameTime += deltaTime;

	angle += deltaTime * 15;

	if (angle > 360) angle -= 360;

	objectTransform.RotateX(deltaTime * 15);
	objectTransform.RotateY(deltaTime * 45);
}

void Game::draw()
{
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// activate and bind texture
	glActiveTexture(GL_TEXTURE0);
	tex.Bind();

	//// TRIANGLE PRIMITIVE DEMO ////
	if (isTriangleUsed) TriangleTest(false);
	//// END OF TRIANGLE PRIMITIVE DEMO ////
	else
	{
		// change active object based on the index
		objectActive = &objectPrimitive[objectActiveIndex];

		// change shader program based on display mode
		if (OBJECT_DISPLAY_NORMAL == displayMode) progActiveSP = &progSimpleDisplay;
		else if (OBJECT_DISPLAY_MODIFIED == displayMode) progActiveSP = &progGeometryAlter;
		else if (OBJECT_DISPLAY_BOTH == displayMode)
		{
			mat4 tempTransform1, tempTransform2;
			tempTransform1.RotateX(angle);
			tempTransform1.RotateY(angle);
			tempTransform1.Translate(vec3(2.0f, 0.0f, 0.0f));
			
			// activate simple shader program
			progActiveSP = &progSimpleDisplay;

			// bind shader program, and send uniforms
			progActiveSP->Bind();
			progActiveSP->SendUniformMat4("uModel", tempTransform1.data, true);
			progActiveSP->SendUniformMat4("uView", cameraTransform.GetInverse().data, true);
			progActiveSP->SendUniformMat4("uProj", cameraProjection.data, true);
			progActiveSP->SendUniform("uTex", 0);

			glBindVertexArray(objectActive->VAO);
			glDrawArrays(GL_TRIANGLES, 0, objectActive->GetNumVertices());
			glBindVertexArray(0);

			progActiveSP->UnBind();

			tempTransform2.RotateX(angle);
			tempTransform2.RotateY(angle);
			tempTransform2.Translate(vec3(-2.0f, 0.0f, 0.0f));
			objectTransform = tempTransform2;

			// change active shader program
			progActiveSP = &progGeometryAlter;
		}

		// bind shader program, and send uniforms
		progActiveSP->Bind();
		progActiveSP->SendUniformMat4("uModel", objectTransform.data, true);
		progActiveSP->SendUniformMat4("uView", cameraTransform.GetInverse().data, true);
		progActiveSP->SendUniformMat4("uProj", cameraProjection.data, true);
		progActiveSP->SendUniform("uTex", 0);

		// additional uniforms for different purpose
		if (OBJECT_DISPLAY_MODIFIED == displayMode || OBJECT_DISPLAY_BOTH == displayMode)
		{
			yGeomMod = angle / 360;												// dynamically adjust offset
			progActiveSP->SendUniform("uYmod", yGeomMod);						// offset on Y axis
			progActiveSP->SendUniform("uIsDuplicated", isDuplicated);			// if object will be duplicated
			progActiveSP->SendUniform("uIsInverted", isInverted);				// if object will be duplicated and then (duplicate) inverted 
		}

		glBindVertexArray(objectActive->VAO);
		glDrawArrays(GL_TRIANGLES, 0, objectActive->GetNumVertices());
		glBindVertexArray(0);

		progActiveSP->UnBind();

		// if there is a need for special feature then display it by using third shader program
		if (isShowWireFrame || hasNormals || hasFaceNormals)
		{
			progActiveSP = &progGeometryFeatures;
			progActiveSP->Bind();
			progActiveSP->SendUniformMat4("uModel", objectTransform.data, true);
			progActiveSP->SendUniformMat4("uView", cameraTransform.GetInverse().data, true);
			progActiveSP->SendUniformMat4("uProj", cameraProjection.data, true);
			progActiveSP->SendUniform("uTex", 0);

			progActiveSP->SendUniform("uIsShowWireFrame", isShowWireFrame);		// should wireframe be displayed?
			progActiveSP->SendUniform("uHasNormals", hasNormals);				// will normals will be displayed on each vertex?
			progActiveSP->SendUniform("uHasFaceNormals", hasFaceNormals);		// will normals be displayed on each face?
			progActiveSP->SendUniform("uNormalLength", normalLength);			// what is the size of line representing normal i.e. normal line length

			glBindVertexArray(objectActive->VAO);
			glDrawArrays(GL_TRIANGLES, 0, objectActive->GetNumVertices());
			glBindVertexArray(0);

			progActiveSP->UnBind();
		}

		tex.Unbind();
	}
	glutSwapBuffers();
}

void Game::keyboardDown(unsigned char key, int mouseX, int mouseY)
{
	switch(key)
	{
	case 27: // the escape key
	case 'q': // the 'q' key
		exit(1);
		break;
	}
}

void Game::keyboardUp(unsigned char key, int mouseX, int mouseY)
{
	float lengthIncrement = 0.1f;

	switch(key)
	{
	case '0':	// 48
	case '-':
		isTriangleUsed = true;
		DisplayUserInfo();
		break;
	case '1':	// 49
	case '2':	// 50
	case '3':	// 51
	case '4':	// 52
	case '5':	// 53
	case '6':	// 54
		isTriangleUsed = false;
		objectActiveIndex = key - 49;
		//std::cout << "Active Object: " << objectActiveIndex << std::endl;
		DisplayUserInfo();
		break;
	case 'b':
		displayMode = OBJECT_DISPLAY_BOTH;
		DisplayUserInfo();
		break;
	case 'n':
		displayMode = OBJECT_DISPLAY_NORMAL;
		objectTransform = mat4::Identity();
		DisplayUserInfo();
		break;
	case 'm':
		displayMode = OBJECT_DISPLAY_MODIFIED;
		objectTransform = mat4::Identity();
		DisplayUserInfo();
		break;
	case 'i':
		SetObjectModifications(OBJECT_MODIFY_INVERT);
		break;
	case 'o':
		SetObjectModifications(OBJECT_MODIFY_DUPLICATE);
		break;
	case 'u':
		SetObjectModifications(OBJECT_MODIFY_BOTH);
		break;
	case 'c':
		AddObjectFeatures(OBJECT_ADD_NOTHING);
		break;
	case 'v':
		AddObjectFeatures(OBJECT_ADD_NORMALS);
		break;
	case 'f':
		AddObjectFeatures(OBJECT_ADD_FACE_NORMALS);
		break;
	case 'w':
		AddObjectFeatures(OBJECT_ADD_WIREFRAME);
		break;
	case '.':
		normalLength += lengthIncrement;
		if (normalLength > 10.0f) normalLength = 10.0f;
		break;
	case ',':
		normalLength -= lengthIncrement;
		if (normalLength < 0.1f) normalLength = 0.1f;
	case 32: // the space bar
		break;
	case 27: // the escape key
	case 'q': // the 'q' key
		exit(1);
		break;
	}
}

void Game::mouseClicked(int button, int state, int x, int y)
{
	if(state == GLUT_DOWN) 
	{
		switch(button)
		{
		case GLUT_LEFT_BUTTON:

			break;
		case GLUT_RIGHT_BUTTON:
		
			break;
		case GLUT_MIDDLE_BUTTON:

			break;
		}
	}
	else
	{

	}
}

/*
 * mouseMoved(x,y)
 * - this occurs only when the mouse is pressed down
 *   and the mouse has moved.  you are given the x,y locations
 *   in window coordinates (from the top left corner) and thus 
 *   must be converted to screen coordinates using the screen to window pixels ratio
 *   and the y must be flipped to make the bottom left corner the origin.
 */
void Game::mouseMoved(int x, int y)
{
}