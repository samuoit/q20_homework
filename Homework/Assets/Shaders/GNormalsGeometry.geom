#version 430

layout(triangles) in;
layout(line_strip, max_vertices = 64) out;

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;

uniform bool uIsShowWireFrame = false;	// show wireframe instead of full geometry?
uniform bool uHasNormals = false;		// are normals displayed for the vertices
uniform bool uHasFaceNormals = false;	// are normals displayed for the faces
uniform float uNormalLength = 0.25;		// length of line representing normal

mat4 mvp;

// Input from Vertex shader
in VertexData
{
	vec3 normal;
	vec2 texCoord;
} vIn[];

// Output from geometry shader
out VertexData
{
	vec3 normal;
	vec2 texCoord;
	vec3 color;
} vOut;


// Create normals per each vertex
void GenerateNormals()
{
	for (int i = 0; i < 3; i++)
	{
		// Normal base (origin)
		gl_Position = mvp * gl_in[i].gl_Position;
		vOut.color = vec3(1, 1, 0);
		EmitVertex();

		// Normal end (finish)
		vec3 normalEnd = gl_in[i].gl_Position.xyz + vIn[i].normal * uNormalLength;
		gl_Position = mvp * vec4(normalEnd, 1);
		vOut.color = vec3(1, 1, 0);
		EmitVertex();
	
		EndPrimitive();
	}
}

// Outputs face normals
void GenerateFaceNormals()
{
	vec3 u = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
	vec3 v = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;
	
	vec3 fNorm = cross(u, v);
	
	vec3 xsect = (gl_in[0].gl_Position.xyz + gl_in[1].gl_Position.xyz + gl_in[2].gl_Position.xyz) / 3.0;

	gl_Position = mvp * vec4(xsect, 1.0);
	vOut.color = vec3(1.0);
	EmitVertex();

	vec3 normalEnd = xsect + normalize(fNorm) * uNormalLength;
	gl_Position = mvp * vec4(normalEnd, 1.0);
	vOut.color = vec3(1.0);
	EmitVertex();

	EndPrimitive();
}

void DisplayWireFrame()
{
	vOut.color = vec3(0.0);

	// Wireframe
	gl_Position = mvp * vec4((gl_in[0].gl_Position.xyz + vIn[0].normal.xyz * 0.0029), 1.0);
	EmitVertex();
	gl_Position = mvp * vec4((gl_in[1].gl_Position.xyz + vIn[1].normal.xyz * 0.0029), 1.0);
	EmitVertex();
	EndPrimitive();

	gl_Position = mvp * vec4((gl_in[0].gl_Position.xyz + vIn[0].normal.xyz * 0.0029), 1.0);
	EmitVertex();
	gl_Position = mvp * vec4((gl_in[2].gl_Position.xyz + vIn[2].normal.xyz * 0.0029), 1.0);
	EmitVertex();
	EndPrimitive();

	gl_Position = mvp * vec4((gl_in[1].gl_Position.xyz + vIn[1].normal.xyz * 0.0029), 1.0);
	EmitVertex();
	gl_Position = mvp * vec4((gl_in[2].gl_Position.xyz + vIn[2].normal.xyz * 0.0029), 1.0);
	EmitVertex();
	EndPrimitive();
}

void main()
{
	mvp = uProj * uView * uModel;	// Model-View-Projection combined matrix

	// Display feature per request beased on uniform set flags
	if (uIsShowWireFrame == true) DisplayWireFrame();
	if (uHasNormals == true) GenerateNormals();
	if (uHasFaceNormals == true) GenerateFaceNormals();
}
