#version 430

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;

layout(location = 0) in vec3 in_vert;	// 0
layout(location = 1) in vec2 in_uv;		// 1
layout(location = 2) in vec3 in_normal;	// 2

out VertexData
{
	vec3 normal;
	vec2 texCoord;
} vOut;

void main()
{
	vOut.normal = in_normal;
	vOut.texCoord = in_uv;
	gl_Position = vec4(in_vert, 1.0);
}