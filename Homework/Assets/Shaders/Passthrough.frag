#version 430

uniform sampler2D uTex;

in vec2 texCoord;

layout (location = 0) out vec4 outColor;

void main()
{
	outColor = texture(uTex, texCoord);
}