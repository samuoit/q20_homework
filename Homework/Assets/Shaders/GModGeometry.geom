#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 12) out;

uniform mat4 uModel;					// model matrix (object vertex transform)
uniform mat4 uView;						// view matrix (viewer - camera transform)
uniform mat4 uProj;						// projection matrix

uniform float uYmod = 0.5;				// vertical Y offset
uniform bool uIsDuplicated = false;		// is object duplicated (with offset)?
uniform bool uIsInverted = false;		// is object duplicated (with invert)?

// Input from vertex shader
in VertexData
{
	vec3 normal;
	vec2 texCoord;
} vIn[];

// Output from geometry shader
out VertexData
{
	vec3 normal;
	vec3 color;
	vec2 texCoord;
} vOut;

mat4 mvp;

// Pass through original geometry
void PassThru()
{
	for(int i = 0; i < 3; i++)
	{
		gl_Position = mvp * gl_in[i].gl_Position;
		vOut.texCoord = (mvp * gl_in[i].gl_Position).xy;
		vOut.color = vec3(1, 0, 0);
		EmitVertex();
	}

	EndPrimitive();
}

// Duplicate geometry
void DuplicateGeometry()
{
	for(int i = 0; i < 3; i++)
	{
		vec3 offset = gl_in[i].gl_Position.xyz  + vec3(0, uYmod, 0);

		gl_Position = mvp * vec4(offset, 1.0);
		vOut.texCoord = (mvp * vec4(offset, 1.0)).xy;
		vOut.color = vec3(0, 1, 0);
		EmitVertex();
	}

	EndPrimitive();
}

// Invert geometry on Y axis - distance specified by uYmod
void InvertGeometry()
{
	for(int i = 0; i < 3; i++)
	{
		vec3 offset = -(gl_in[i].gl_Position.xyz  + vec3(0, uYmod, 0));

		gl_Position = mvp * vec4(offset, 1.0);
		vOut.texCoord = (mvp * vec4(offset, 1.0)).xy;
		vOut.color = vec3(0, 0, 1);
		EmitVertex();
	}

	EndPrimitive();
}

void main()
{
	// model-view-projection matrix combined
	mvp = uProj * uView * uModel;

	PassThru();											// pass geometry as it is
	
	// duplicate geometry (inverted duplication / offset duplication)
	if (uIsInverted == true) InvertGeometry();			// Invert duplicated geometry (also duplicates object first)
	if (uIsDuplicated == true) DuplicateGeometry();		// Duplicate geometry
	
}