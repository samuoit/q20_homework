#version 430

uniform sampler2D uTex;

in VertexData
{
	vec3 normal;
	vec3 color;
	vec2 texCoord;
} vIn;

layout (location = 0) out vec4 outColor;

void main()
{
	// outColor = vec4(vIn.color, 1.0);
	outColor = texture(uTex, vIn.texCoord);
}